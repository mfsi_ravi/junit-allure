package com.mfsi;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.mfsi.util.ExtentManager;
import com.mfsi.util.ExtentTestLogRule;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorPowerMock.class)
public class CalculatorPowerMockTest {

    private static ExtentReports extent;
    private ExtentTest test;

    @Rule
    public ExtentTestLogRule extentTestLogRule = new ExtentTestLogRule();

    @BeforeClass
    public static void setup() {
        extent = ExtentManager.getInstance();
    }

    @Test
    public void testAdd() {
        // Mocking static method
        mockStatic(CalculatorPowerMock.class);
        when(CalculatorPowerMock.add(2, 3)).thenReturn(5);

        // Calling the static method under test
        int result = CalculatorPowerMock.add(2, 3);

        // Verifying the result
        assertEquals(5, result);
    }

    @Test
    public void testMultiply() throws Exception {
        // Mocking private method
        mockStatic(CalculatorPowerMock.class);

        when(CalculatorPowerMock.class, "multiply", 2, 3).thenReturn(6);

        // Using PowerMockito to invoke the private method
        int result = Whitebox.invokeMethod(CalculatorPowerMock.class, "multiply", 2, 3);

        // Verifying the result
        assertEquals(6, result);
    }

    @AfterClass
    public static void tearDown() {
        extent.flush();
    }
}