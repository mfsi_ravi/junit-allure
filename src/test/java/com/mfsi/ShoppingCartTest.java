package com.mfsi;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.mfsi.util.ExtentManager;
import com.mfsi.util.ExtentTestLogRule;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

public class ShoppingCartTest {

    private static ExtentReports extent;
    private ExtentTest test;

    @Rule
    public ExtentTestLogRule extentTestLogRule = new ExtentTestLogRule();

    @BeforeClass
    public static void setup() {
        extent = ExtentManager.getInstance();
    }

    @Test
    public void testAddItem() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem("Apple");
        cart.addItem("Banana");
        Assert.assertTrue(cart.containsItem("Apple"));
        Assert.assertTrue(cart.containsItem("Banana"));
        Assert.assertEquals(2, cart.getItemCount());
    }

    @Test
    public void testRemoveItem() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem("Apple");
        cart.addItem("Banana");
        cart.removeItem("Apple");
        Assert.assertFalse(cart.containsItem("Apple"));
        Assert.assertTrue(cart.containsItem("Banana"));
        Assert.assertEquals(1, cart.getItemCount());
    }

    @Test
    public void testClearCart() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem("Apple");
        cart.addItem("Banana");
        cart.clearCart();
        Assert.assertEquals(0, cart.getItemCount());
    }

    @AfterClass
    public static void tearDown() {
        extent.flush();
    }
}
