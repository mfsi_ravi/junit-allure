package com.mfsi.util;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.junit.Assert;
import org.junit.AssumptionViolatedException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class ExtentTestLogRule extends TestWatcher {

    private ExtentTest test;

    @Override
    protected void succeeded(Description description) {
        test.log(Status.PASS, "Test passed");
    }

    @Override
    protected void failed(Throwable e, Description description) {
        test.log(Status.FAIL, "Test failed: " + e.getMessage());
    }

    @Override
    protected void skipped(AssumptionViolatedException e, Description description) {
        test.log(Status.SKIP, "Test skipped: " + e.getMessage());
    }

    @Override
    protected void starting(Description description) {
        ExtentReports extent = ExtentManager.getInstance();
        test = extent.createTest(description.getClassName() + "::" + description.getMethodName());
    }
}
