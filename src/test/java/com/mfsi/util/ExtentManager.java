package com.mfsi.util;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public final class ExtentManager {

    private static final ExtentSparkReporter sparkReporter = new ExtentSparkReporter("extent-report.html");
    private static final ExtentReports extent = new ExtentReports();

    public static ExtentReports getInstance() {
        extent.attachReporter(sparkReporter);
        return extent;
    }

    private ExtentManager() {
        super();
    }


}
