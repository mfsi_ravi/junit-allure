package com.mfsi;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.mfsi.util.ExtentManager;
import com.mfsi.util.ExtentTestLogRule;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CalculatorTest {

    private static ExtentReports extent;
    private ExtentTest test;

    @Rule
    public ExtentTestLogRule extentTestLogRule = new ExtentTestLogRule();

    @BeforeClass
    public static void setup() {
        extent = ExtentManager.getInstance();
    }

    @Test
    public void testAdd() {
        Calculator calculator = new Calculator();
        int result = calculator.add(2, 3);
        Assert.assertEquals(5, result);
    }

    @Test
    public void testSubtract() {
        Calculator calculator = new Calculator();
        int result = calculator.subtract(5, 3);
        Assert.assertEquals(2, result);
    }

    @Test
    public void testMultiply() {
        Calculator calculator = new Calculator();
        int result = calculator.multiply(2, 3);
        Assert.assertEquals(6, result);
    }

    @Test
    public void testDivide() {
        Calculator calculator = new Calculator();
        double result = calculator.divide(10, 2);
        Assert.assertEquals(5.0, result, 0.0001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDivideByZero() {
        Calculator calculator = new Calculator();
        calculator.divide(10, 0);
    }

    @Test
    public void testSumOfPositiveIntegers() {
        Calculator calculator = new Calculator();
        assertEquals(5, calculator.add(2, 3));
    }

    @Test
    @Ignore("Skipping this test for demonstration")
    public void testMultiplication() {
        // This test will be skipped
        fail();
    }

    @Test
    @Ignore("Skipping this test for demonstration")
    public void testFailedCase() {
        // Failed for demonstration
        fail();
    }

    @AfterClass
    public static void tearDown() {
        extent.flush();
    }
}