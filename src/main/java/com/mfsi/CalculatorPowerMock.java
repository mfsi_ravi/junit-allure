package com.mfsi;

public class CalculatorPowerMock {

    private CalculatorPowerMock() {
        super();
    }

    public static int add(int a, int b) {
        return a + b;
    }

    private static int multiply(int a, int b) {
        return a * b;
    }
}